package ficha5_grupo2;

import java.util.Scanner;

public class Ficha5_grupo2 {

    public static void main(String[] args) {
        int idade;
        double salario, taxae, taxaf, total, sair = 0;
        while (sair != -1) {
            Scanner ler = new Scanner(System.in);
            System.out.println("Insira o seu salário");
            salario = ler.nextInt();
            System.out.println("Introduza a sua idade");
            idade = ler.nextInt();

            if (idade <= 55) {
                if (salario > 6000) {
                    taxae = 6000 * 0.2;
                    taxaf = 6000 * 0.17;
                    System.out.println("A contribuição paga pela empresa é: " + taxae);
                    System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                    total = taxaf + taxae;
                    System.out.println("A contribuição total é: " + total);
                }
                taxae = salario * 0.2;
                taxaf = salario * 0.17;
                System.out.println("A contribuição paga pela empresa é: " + taxae);
                System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                total = taxaf + taxae;
                System.out.println("A contribuição total é: " + total);
            } else if (idade <= 60) {
                if (salario > 6000) {
                    taxae = 6000 * 0.13;
                    taxaf = 6000 * 0.13;
                    System.out.println("A contribuição paga pela empresa é: " + taxae);
                    System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                    total = taxaf + taxae;
                    System.out.println("A contribuição total é: " + total);
                }
                taxae = salario * 0.13;
                taxaf = salario * 0.13;
                System.out.println("A contribuição paga pela empresa é: " + taxae);
                System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                total = taxaf + taxae;
                System.out.println("A contribuição total é: " + total);
            } else if (idade <= 65) {
                if (salario > 6000) {
                    taxae = 6000 * 0.075;
                    taxaf = 6000 * 0.09;
                    System.out.println("A contribuição paga pela empresa é: " + taxae);
                    System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                    total = taxaf + taxae;
                    System.out.println("A contribuição total é: " + total);
                }
                taxae = salario * 0.075;
                taxaf = salario * 0.09;
                System.out.println("A contribuição paga pela empresa é: " + taxae);
                System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                total = taxaf + taxae;
                System.out.println("A contribuição total é: " + total);
            } else if (idade > 65) {
                if (salario > 6000) {
                    taxae = 6000 * 0.05;
                    taxaf = 6000 * 0.075;
                    System.out.println("A contribuição paga pela empresa é: " + taxae);
                    System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                    total = taxaf + taxae;
                    System.out.println("A contribuição total é: " + total);
                }
                taxae = salario * 0.05;
                taxaf = salario * 0.075;
                System.out.println("A contribuição paga pela empresa é: " + taxae);
                System.out.println("A contribuição paga pelo funcionario é: " + taxaf);
                total = taxaf + taxae;
                System.out.println("A contribuição total é: " + total);

            }
            System.out.println(" ");
            System.out.println("Se pretender sair, digite '-1', se pretender continuar digite qualquer numero.");
            sair = ler.nextInt();
        }
    }

}
