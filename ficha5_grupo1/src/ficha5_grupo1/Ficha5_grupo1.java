package ficha5_grupo1;

import java.util.Scanner;

public class Ficha5_grupo1 {

    public static void main(String[] args) {
        int inicial = 0, fin = 0;
        while (fin - inicial < 100) {
            Scanner ler = new Scanner(System.in);
            System.out.println("Insira o numero inicial");
            inicial = ler.nextInt();
            System.out.println("Insira o numero final");
            fin = ler.nextInt();
        }
            for (int i = inicial; i < fin; i++) {

                if (i % 5 == 0 && i % 7 == 0) {
                    System.out.print(" ISEPIPP ");
                } else if (i % 3 == 0 && i % 7 == 0) {
                    System.out.print(" FEIRAIPP ");
                } else if (i % 3 == 0 && i % 5 == 0) {
                    System.out.print(" FEIRAISEP ");
                } else if (i % 7 == 0) {
                    System.out.print(" IPP ");
                } else if (i % 5 == 0) {
                    System.out.print(" ISEP ");
                } else if (i % 3 == 0) {
                    System.out.print(" FEIRA ");
                } else {
                    System.out.print(i);
                }
                System.out.print(" ");

            }
        
    }

}
