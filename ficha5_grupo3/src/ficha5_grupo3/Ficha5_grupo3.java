package ficha5_grupo3;

import java.util.Scanner;

public class Ficha5_grupo3 {

    public static void main(String[] args) {
        int linhas = 0, colunas = 0, i, j, cont = 0, cont1 = 0;
        int matriz[][] = new int[100][100];
        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o numero de linhas para a matriz");
        linhas = ler.nextInt();
        System.out.println("Introduza o numero de colunas para a matriz");
        colunas = ler.nextInt();

        System.out.println("Introduza os numeros da matriz");
        for (i = 0; i < linhas; i++) {
            for (j = 0; j < colunas; j++) {
                matriz[i][j] = ler.nextInt();
            }
        }
        System.out.println("A matriz é:");

        for (int contador = 0; contador < colunas; contador++) {
            System.out.print(" "+"|");
            System.out.print(cont1);
            cont1 = cont1 + 1;
        }
        System.out.println();
        for (i = 0; i < linhas; i++) {

            System.out.print(cont);
            System.out.print("| ");
            cont = cont + 1;
            for (j = 0; j < colunas; j++) {
                System.out.print(" "+matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}
